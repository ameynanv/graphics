#ifndef _RENDER_H_
#define _RENDER_H_

float SQ(float num);
void drawScene();
void drawAxes(float width);
void drawCircle(float width);
void drawText(int x, int y, float r, float g, float b, void* font, char * name);

#endif
