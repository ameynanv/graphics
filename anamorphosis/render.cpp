#include "render.h"
#include <iostream>
#include <vector>
#include "anamorph.h"
#include "math.h"

float SQ(float num) {return num*num;}

void transformPlanerInfinite(float& xTex, float& yTex, float& xObj, float& yObj, float angle) {
	double angleRad = angle / 180.00 * M_PI;
	xObj = (0.5 - yTex) / sin(angleRad);
	yObj = (0.5 - xTex);
}

void transformPlanerFinite(float& xTex, float& yTex, float& xObj, float& yObj, float h, float d, float angle) {
	double angleRad = angle / 180.00 * M_PI;
	float x = (0.5 - yTex);
	float y = (0.5 - xTex);
	xObj = (x/sin(angleRad))/(1-(x/h)*cos(angleRad)) * 2;
	yObj = y * (sqrt(SQ(h) + SQ(d+xObj)))/(sqrt(SQ(h)+SQ(d)+SQ(x))) * 2;
}

void transformConicalInfinite(float& xTex, float& yTex, float& xObj, float& yObj, float coneRadius, float coneAngle) {
	// xTex, yTex is in the range 0 to 1
	float x = -1*(0.5 - yTex);
	float y = -1*(0.5 - xTex);
	// x,y is now in the range -0.5 to +0.5
	float theta = atan2(y , x);
	float theta_deg = theta/M_PI*180.00;
	float r = sqrt(SQ(x) + SQ(y)) / (sqrt(SQ(0.5) + SQ(0.5)));
	float h = 1.0 / tan((coneAngle / 180.00 * M_PI) / 2.00);
	float r_new = r + 2.0 * (1 - r) * SQ(h) / (SQ(h) - 1);
	xObj = r_new * cos(theta);
	yObj = r_new * sin(theta);
	//printf("Input: ( %.3f %.3f ) Output: ( %.3f %.3f ) \n", x, y , xObj, yObj);
}

void transformCylindricalInfinite(float& xTex, float& yTex, float& xObj, float& yObj, float angle){
    float x = (0.5 - yTex);
    float y = -1*(0.5 - xTex);
    double angleRad = angle/ 180.00 * M_PI;
    //double angleRad=Theta;
    xObj=2*pow(1-SQ(y),1.5)+x*(1-2*SQ(y))/sin(angleRad);
    yObj=y*(3-2*SQ(y)+2*x*(sqrt(1-SQ(y)))/sin(angleRad));
}

void transformCylindricalFinite(float& xTex, float& yTex, float& xObj, float& yObj, float h, float d,float angle){
    float x = (0.5 - yTex);
    float y = -1*(0.5 - xTex);
    double angleRad = angle/ 180.00 * M_PI;
    //double angleRad=Theta;
    float x_prime = (x/sin(angleRad))/(1-(x/h)*cos(angleRad));
    float y_prime = y * (sqrt(SQ(h) + SQ(d+x_prime)))/(sqrt(SQ(h)+SQ(d)+SQ(x)));
    float tmp=(d*SQ(y_prime)+(d+x_prime)*sqrt(SQ(d)*(1-SQ(y_prime))+2*d*x_prime+SQ(y_prime)+SQ(x_prime)))/(SQ(d+x_prime)+SQ(y_prime));
    xObj=-x_prime+2*tmp*(tmp+x_prime)*((d*tmp-1)/(d-tmp));
    yObj=y_prime+(2*y_prime/(d+x_prime))*(tmp+x_prime)*(d*tmp-1);
}

void anamorphicTransform(float& xTex, float& yTex, float& xObj, float& yObj, Setup * setup) {
	switch(setup->type) {
		case PLANER_FINITE:
		{
			float h  = setup->R_setpoint * sin(setup->Theta_setpoint * M_PI/180.0);
			float d  = setup->R_setpoint * cos(setup->Theta_setpoint * M_PI/180.0);
			transformPlanerFinite(xTex, yTex, xObj, yObj, h, d, setup->Theta_setpoint);
			break;
		}
		case PLANER_INFINITE:
		{
			transformPlanerInfinite(xTex, yTex, xObj, yObj, setup->Theta_setpoint);
			break;
		}
		case CONICAL_INFINITE:
		{
			transformConicalInfinite(xTex, yTex, xObj, yObj, setup->cone_radius, setup->cone_angleSetpoint);
			break;
		}
		case CYLINDRICAL_INFINITE:
		{
            transformCylindricalInfinite(xTex, yTex, xObj, yObj, setup->cylinder_angleSetpoint);
			break;
		}
        case CYLINDRICAL_FINITE:
        {
            float h  = setup->R_setpoint * sin(setup->Theta_setpoint * M_PI/180.0);
            float d  = setup->R_setpoint * cos(setup->Theta_setpoint * M_PI/180.0);
            transformCylindricalFinite(xTex, yTex, xObj, yObj, h, d, setup->Theta_setpoint);
            break;
        }
		default:
			break;
	}
}

void cartesianGridBasedRendering() {
	float xTex, yTex, xObj, yObj;
	int N = 100;
    for (int i=0; i < N; i++) {
    	for (int j=0; j < N; j++) {
        	float step = 1.0/(N*1.0);
    		float x = i * step;
    		float y = j * step;
    		glBegin(GL_TRIANGLES);
    			glNormal3f(0.0,0.0,-1.0);
    			xTex = x; yTex = y;
    			anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);

				xTex = x; yTex = y + 1.0/(N*1.0);
				anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);

				xTex = x + 1.0/(N*1.0); yTex = y + 1.0/(N*1.0);
				anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
			glEnd();
    		glBegin(GL_TRIANGLES);
    			glNormal3f(0.0,0.0,-1.0);
    			xTex = x; yTex = y;
    			anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);

				xTex = x + 1.0/(N*1.0); yTex = y;
				anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);

				xTex = x + 1.0/(N*1.0); yTex = y + 1.0/(N*1.0);
				anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
				glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
			glEnd();
    	}
    }
}

void sphericalGridBasedRendering() {
	float xTex, yTex, xObj, yObj;
	float r = 0, t = 0;
	int RStep = 1000;
	for (r = 1.0/(RStep*1.0); r < 1.0; r = r + 1.0/(RStep*1.0)) {
		for (t = 0; t < 360.0; t = t + 0.5) {
			float x1 = r * cos(t/180.0*M_PI);
			float y1 = r * sin(t/180.0*M_PI);
			float x2 = (r + 1.0/(RStep*1.0)) * cos(t/180.0*M_PI);
			float y2 = (r + 1.0/(RStep*1.0)) * sin(t/180.0*M_PI);
			float x3 = (r + 1.0/(RStep*1.0)) * cos((t+1)/180.0*M_PI);
			float y3 = (r + 1.0/(RStep*1.0)) * sin((t+1)/180.0*M_PI);
			float x4 = r * cos((t+1)/180.0*M_PI);
			float y4 = r * sin((t+1)/180.0*M_PI);
			if ((x1 <= 0.5 && x1 >= -0.5 && y1 <= 0.5 && y1 >= -0.5) &&
				(x2 <= 0.5 && x2 >= -0.5 && y2 <= 0.5 && y2 >= -0.5) &&
				(x3 <= 0.5 && x3 >= -0.5 && y3 <= 0.5 && y3 >= -0.5) &&
				(x4 <= 0.5 && x4 >= -0.5 && y4 <= 0.5 && y4 >= -0.5))
			{
				glBegin(GL_TRIANGLES);
					glNormal3f(0.0,0.0,-1.0);
					xTex = x1 + 0.5; yTex = y1 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
					xTex = x2 + 0.5; yTex = y2 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
					xTex = x3 + 0.5; yTex = y3 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
				glEnd();
				glBegin(GL_TRIANGLES);
					glNormal3f(0.0,0.0,-1.0);
					xTex = x1 + 0.5; yTex = y1 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
					xTex = x4 + 0.5; yTex = y4 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
					xTex = x3 + 0.5; yTex = y3 + 0.5;
					anamorphicTransform(xTex, yTex, xObj, yObj, &setup);
					glTexCoord2f(xTex, yTex); glVertex3f( xObj,	yObj,	0.0);
				glEnd();
			}
		}
	}
}

void drawCircle(float radius) {
	glColor3f(0.0, 0.0, 0.0);
	for (float t = 0; t < 360.0; t = t + 5) {
		glLineWidth(1.0);
		glBegin(GL_LINES);
			float x1 = radius * cos(t/180.00*M_PI);
			float y1 = radius * sin(t/180.00*M_PI);
			float x2 = radius * cos((t+5)/180.00*M_PI);
			float y2 = radius * sin((t+5)/180.00*M_PI);
			glVertex3f(x1,y1,0.0);
			glVertex3f(x2,y2,0.0);
		glEnd();
	}
}

void drawScene() {

	switch(setup.type) {
		case CONICAL_INFINITE:
		case CONICAL_FINITE:
		{
			sphericalGridBasedRendering();
			glDisable(GL_TEXTURE_2D);
			//drawCircle(1.0);
			break;
		}
        case CYLINDRICAL_FINITE:
        case CYLINDRICAL_INFINITE:
        {
            cartesianGridBasedRendering();
            glDisable(GL_TEXTURE_2D);
            //drawCircle(1.0);
            break;
        }
		default:
		{
			cartesianGridBasedRendering();
			break;
		}
	}
	/*
	glBegin(GL_LINES);
		glColor4f(0.0, 0.0, 0.0,0.0);
		glVertex3f(setup.textureScale/2.0,setup.textureScale/2.0,0.0);
		glVertex3f(-setup.textureScale/2.0,setup.textureScale/2.0,0.0);

		glVertex3f(-setup.textureScale/2.0,setup.textureScale/2.0,0.0);
		glVertex3f(-setup.textureScale/2.0,-setup.textureScale/2.0,0.0);

		glVertex3f(-setup.textureScale/2.0,-setup.textureScale/2.0,0.0);
		glVertex3f(setup.textureScale/2.0,-setup.textureScale/2.0,0.0);

		glVertex3f(setup.textureScale/2.0,-setup.textureScale/2.0,0.0);
		glVertex3f(setup.textureScale/2.0, setup.textureScale/2.0,0.0);
	glEnd();
	*/
}


void drawAxes(float width) {
	glLineWidth(2);
	glBegin(GL_LINES);
	// X axis: Red
	glColor4f(1.0, 0.0, 0.0,0.0);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(width,0.0,0.0);

	// Y axis: Green
	glColor4f(0.0, 1.0, 0.0, 0.0);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,width,0.0);

	// Z axis: Blue
	glColor4f(0.0, 0.0, 1.0,0.0);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,0.0,width);

	glEnd();
}

void drawText(int x, int y, float r, float g, float b, void* font, char * name)
{
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D( 0, 640, 0, 480 );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glColor3f( r, g, b );
	int len = (int)strlen(name);
	glRasterPos2i(x , y);
	for ( int i = 0; i < len; ++i ) {
		glutBitmapCharacter(font, name[i]);
	}
	glPopMatrix();

	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}
