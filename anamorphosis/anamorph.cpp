#include "anamorph.h"
#include "render.h"
#include "input.h"
#include "performanceCounter.h"

using namespace std;

GLubyte * textureImage;
GLuint texName;

char* fileName;
char* outputFileName;

PerformanceCounter counter;

// camera parameters
double Theta;
double Phi;
double R;

// mouse control
int g_iMenuId;
int g_vMousePos[2];
int g_iLeftMouseButton,g_iMiddleMouseButton,g_iRightMouseButton;
int g_iLeftMouseButtonDown, g_iLeftMouseButtonUp;
int mouseOrigin[2];

// this is used for deciding frame rate
int frameCounter = 0;

// number of images saved to disk so far
int sprite=0;

// these variables control what is displayed on screen
int saveScreenToFile=1;

bool debugMode;

Setup setup;

void createTextureImage(void)
{
   Pic *pic = ppm_read(fileName, NULL);
   printf("Size of image => %d, %d , Bytes per pixel =  %d\n", pic->nx, pic->ny, pic->bpp);
   textureImage = (GLubyte *)malloc(sizeof(GLubyte) * pic->nx * pic->ny * pic->bpp);
   for (int j = 0; j < pic->ny; j++) {
    	 for (int i = 0; i < pic->nx; i++) {
    	 for (int k = 0; k < pic->bpp; k++) {
    		 int index = j* pic->nx * pic->bpp + i * pic->bpp + k ;
    	     textureImage[index] = pic->pix[index];
    	 }
      }
   }
}

void initTexture() {
	createTextureImage();

	// create placeholder for texture
	glGenTextures(1, &texName);

	// make texture “texName” the currently active texture
	glBindTexture(GL_TEXTURE_2D, texName);

	// specify texture parameters (they affect whatever texture is active)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // repeat pattern in s
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // repeat pattern in t

	// use linear filter both for magnification and minification
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// load image data stored at pointer “textureImage” into the currently  active texture (“texName”)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, setup.textureWidth, setup.textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, textureImage);
}

void myinit()
{
	// set background color to grey
	glClearColor(1.0, 1.0, 1.0, 0.0);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_LINE_SMOOTH);

	initTexture();
	return;
}

void reshape(int w, int h) 
{
	// Prevent a divide by zero, when h is zero.
	// You can't make a window of zero height.
	if(h == 0)
	h = 1;

	glViewport(0, 0, w, h);

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set the perspective
	double aspectRatio = 1.0 * w / h;
	switch(setup.type) {
		case PLANER_INFINITE:
			glOrtho(-1.5,1.5,-1.5/aspectRatio, 1.5/aspectRatio, 0.01f, 1000.0f);
			break;
		default:
			gluPerspective(setup.fov, aspectRatio, 0.01f, 100.0f);
			break;
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	setup.textureWidth = w;
	setup.textureHeight = h;

	glutPostRedisplay();
}

void printDebugInfo() {
	drawAxes(1.0);
}

void printCommands() {
	char buffer[200];
	sprintf(buffer, "R = %.2f, Theta = %.2f, Phi = %.2f", R, Theta * 180.00/ M_PI, Phi * 180.00/ M_PI);
	drawText(10,70,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "w/s = To increase/decrease Theta");
	drawText(10,50,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "a/d = To increase/decrease Phi");
	drawText(10,30,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "z/x = To increase/decrease R");
	drawText(10,10,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// camera parameters are Phi, Theta, R
	gluLookAt(R * cos(Phi) * cos (Theta), R * sin(Phi) * cos (Theta), R * sin (Theta),	0.0, 0.0, 0.0, 0.0, 0.0, 1.0);

	// GL_DEPTH_TEST will enable Hidden Surface Removal
	glEnable(GL_DEPTH_TEST);

	// no modulation of texture color with lighting; use texture color directly
	glTexEnvf(GL_TEXTURE_ENV,  GL_TEXTURE_ENV_MODE, GL_REPLACE);

	// turn on texture mapping (this disables standard OpenGL lighting,  unless in GL_MODULATE mode)
	glEnable(GL_TEXTURE_2D);

	drawScene();

	if (debugMode) {
		drawCircle(1.0);
	}
	glDisable(GL_TEXTURE_2D);

	glutSwapBuffers();
}

void doIdle()
{
  counter.StopCounter();
  double timePerFrame = counter.GetElapsedTime();
  double frameRate = 1.0 / timePerFrame;
  ostringstream frameRateString;
  frameRateString << frameRate;
  std::string title = "Frame Rate: " + frameRateString.str() + "";
  glutSetWindowTitle(title.c_str());

  counter.StartCounter();
  char s[20]="picxxxx.ppm";
  int i;
  
  // save screen to file
  s[3] = 48 + (sprite / 1000);
  s[4] = 48 + (sprite % 1000) / 100;
  s[5] = 48 + (sprite % 100 ) / 10;
  s[6] = 48 + sprite % 10;
  if (saveScreenToFile==1)
  {
      if (sprite == 0) {
    	  saveScreenshot(setup.textureWidth, setup.textureHeight, outputFileName);
      } else {
    	  saveScreenshot(setup.textureWidth, setup.textureHeight, s);
      }
      sprite++;
      saveScreenToFile=0;
      if (!debugMode) {
    	  exit(0);
      }
  }
  glutPostRedisplay();
}

int main (int argc, char ** argv)
{
  processArgumentsForAnamorph(argc, argv);
  Theta = M_PI/2.0;

  glutInit(&argc,argv);
  
  /* double buffered window, use depth testing, 640x480 */
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  
  glutInitWindowSize (setup.textureWidth, setup.textureHeight);
  glutInitWindowPosition (0,0);

  glutCreateWindow ("Group 0: Anamorphosis");

  /* tells glut to use a particular display function to redraw */
  glutDisplayFunc(display);

  /* replace with any animate code */
  glutIdleFunc(doIdle);

  /* callback for mouse drags */
  glutMotionFunc(mouseMotionDrag);

  /* callback for window size changes */
  glutReshapeFunc(reshape);

  /* callback for mouse movement */
  glutPassiveMotionFunc(mouseMotion);

  /* callback for mouse button changes */
  glutMouseFunc(mouseButton);

  /* register for keyboard events */
  glutKeyboardFunc(keyboardFunc);

  /* do initialization */
  myinit();

  /* forever sink in the black hole */
  glutMainLoop();

  return(0);
}

