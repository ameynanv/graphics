/*
 * rayTracerRenderer.h
 *
 *  Created on: Apr 22, 2015
 *      Author: amey
 */

#ifndef RAYTRACERRENDERER_H_
#define RAYTRACERRENDERER_H_
#include "rayTracer.h"

void rayTraceScene();

#endif /* RAYTRACERRENDERER_H_ */
