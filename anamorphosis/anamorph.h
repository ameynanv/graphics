#ifndef _ANAMORPH_H_
#define _ANAMORPH_H_

#ifdef WIN32
  #include <windows.h>
#endif

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sstream>
#include "openGL-headers.h"
#include "pic.h"

#define pi 3.141592653589793238462643383279 

enum AnamorphType {
	PLANER_INFINITE,
	PLANER_FINITE,
	CONICAL_INFINITE,
	CONICAL_FINITE,
	CYLINDRICAL_INFINITE,
    CYLINDRICAL_FINITE,
};

struct Setup {
	int displayWidth;
	int displayHeight;
	double fov;
	int textureWidth;
	int textureHeight;
	double textureScale;

	double R_setpoint;
	double Theta_setpoint;

	double cone_radius;
	double cone_angleSetpoint;

    double cylinder_angleSetpoint;
    double cylinder_radius;
	AnamorphType type;
};

// camera angles
extern double Theta;
extern double Phi;
extern double R;

// number of images saved to disk so far
extern int sprite;

// mouse control
extern int g_vMousePos[2];
extern int g_iLeftMouseButton,g_iMiddleMouseButton,g_iRightMouseButton;
extern int g_iLeftMouseButtonUp, g_iLeftMouseButtonDown;
extern int mouseOrigin[2];


extern char* fileName;
extern char* outputFileName;

// these variables control what is displayed on the screen
extern int saveScreenToFile;

extern GLuint texName;

extern bool debugMode;

extern Setup setup;

#endif

