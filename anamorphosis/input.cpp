#include "input.h"
#include <iostream>
#include "anamorph.h"

using namespace std;


double getTextureScale(double fovAngle, double r) {
	return 2 * r * tan((fovAngle/2.0)*M_PI/180.00);
}

void processArgumentsForAnamorph(int argc, char ** argv) {
	fileName = (char *)malloc(sizeof(char)*100);
	outputFileName = (char *)malloc(sizeof(char)*100);
	if (argc < 3 || argc > 5) {
		printf("Usage: ./anamorph [InputFileName] [OutputFileName] [AnamorphType] <Debug> \n ./anamorph images/input/Mona_Lisa.ppm images/coneOutput/Mona_Lisa.ppm 3 0\n");
		printf("AnamorphType:\n3: CONICAL_INFINITE\n4: CYLINDRICAL_FINITE\n");
		exit(0);
	}
	sprintf(fileName, "%s", argv[1]);
	sprintf(outputFileName, "%s", argv[2]);

	int type = atoi(argv[3]);
	if(type == 1) setup.type = PLANER_INFINITE;
	if(type == 2) setup.type = PLANER_FINITE;
	if(type == 3) setup.type = CONICAL_INFINITE;
	if(type == 4) setup.type = CYLINDRICAL_FINITE;
	if (argc  == 5) {
		int d = atoi(argv[4]);
		if (d == 1) {debugMode = true;}
		else{ debugMode = false;}
	}

	setup.displayWidth = 1024;
	setup.displayHeight = 768;
	setup.fov = 23;

	// Texture width and height should be power of 2
	setup.textureHeight = 512;
	setup.textureWidth = 512;

	setup.R_setpoint = 15.0;
	setup.Theta_setpoint = 30.0;

	setup.cone_angleSetpoint = 40.0;
	setup.cone_radius = 1.0;

	setup.cylinder_radius =1.0;
	setup.cylinder_angleSetpoint =40.0;

	R = setup.R_setpoint;
	Theta = setup.Theta_setpoint/180.0*M_PI;
	Phi = 0;
	setup.textureScale = getTextureScale(setup.fov, R);
}


void processArgumentsForRayTracer(int argc, char ** argv) {
	fileName = (char *)malloc(sizeof(char)*100);
	if (argc < 2) {
		printf("Usage: ./rayTracer [AnamorphedFileName] [AnamorphType] <Debug> \n ./rayTracer images/coneOutput/Mona_Lisa.ppm 3 1 \n");
		printf("AnamorphType:\n3: CONICAL_INFINITE\n4: CYLINDRICAL_FINITE\n");
		exit(0);
	}
	sprintf(fileName, "%s", argv[1]);
	int type = atoi(argv[2]);
	if(type == 1) setup.type = PLANER_INFINITE;
	if(type == 2) setup.type = PLANER_FINITE;
	if(type == 3) setup.type = CONICAL_INFINITE;
	if(type == 4) setup.type = CYLINDRICAL_FINITE;
	if (argc  == 4) {
		int d = atoi(argv[3]);
		if (d == 1) {debugMode = true;}
		else{ debugMode = false;}
	}

	setup.displayWidth = 1024;
	setup.displayHeight = 768;
	setup.fov = 23;

	// Texture width and height should be power of 2
	setup.textureHeight = 512;
	setup.textureWidth = 512;

	setup.R_setpoint = 15.0;
	setup.Theta_setpoint = 30.0;

	setup.cone_angleSetpoint = 40.0;
	setup.cone_radius = 1.0;

	setup.cylinder_radius =1.0;
	setup.cylinder_angleSetpoint =40.0;

	R = setup.R_setpoint;
	Theta = setup.Theta_setpoint/180.0*M_PI;
	Phi = 0;
	setup.textureScale = getTextureScale(setup.fov, R);

}

/* Write a screenshot, in the PPM format, to the specified filename, in PPM format */
void saveScreenshot(int windowWidth, int windowHeight, char *filename)
{
	if (filename == NULL)
	return;

	// Allocate a picture buffer
	Pic * in = pic_alloc(windowWidth, windowHeight, 3, NULL);
	printf("File to save to: %s\n", filename);

	for (int i=windowHeight-1; i>=0; i--)
	{
	  glReadPixels(0, windowHeight-i-1, windowWidth, 1, GL_RGB, GL_UNSIGNED_BYTE, &in->pix[i*in->nx*in->bpp]);
	}

	if (ppm_write(filename, in))
	  printf("File saved Successfully\n");
	else
	  printf("Error in Saving\n");
	pic_free(in);
}

/* converts mouse drags into information about rotation/translation/scaling */
void mouseMotionDrag(int x, int y)
{
  int vMouseDelta[2] = {x-g_vMousePos[0], y-g_vMousePos[1]};

  if (g_iRightMouseButton) // handle camera rotations
  {
    Phi += vMouseDelta[0] * 0.01;
    Theta += vMouseDelta[1] * 0.01;
    
    if (Phi>2*pi)
      Phi -= 2*pi;
    
    if (Phi<0)
      Phi += 2*pi;
    
    if (Theta>pi / 2 - 0.01) // dont let the point enter the north pole
      Theta = pi / 2 - 0.01;
    
    if (Theta<- pi / 2 + 0.01)
      Theta = -pi / 2 + 0.01;
    
    g_vMousePos[0] = x;
    g_vMousePos[1] = y;
  }
}

void mouseMotion (int x, int y)
{
  g_vMousePos[0] = x;
  g_vMousePos[1] = y;
}

void mouseButton(int button, int state, int x, int y)
{
  switch (button)
  {
    case GLUT_LEFT_BUTTON:
    	g_iLeftMouseButtonDown = (state==GLUT_DOWN);
    	g_iLeftMouseButtonUp = (state==GLUT_UP);
		if (g_iLeftMouseButtonDown) {
		    mouseOrigin[0] = x;
		    mouseOrigin[1] = y;
		}
      break;
    case GLUT_MIDDLE_BUTTON:
      g_iMiddleMouseButton = (state==GLUT_DOWN);
      break;
    case GLUT_RIGHT_BUTTON:
      g_iRightMouseButton = (state==GLUT_DOWN);
      break;
  }
 
  g_vMousePos[0] = x;
  g_vMousePos[1] = y;
}

// gets called whenever a key is pressed
void keyboardFunc (unsigned char key, int x, int y)
{
  switch (key)
  {
    case 27:
      exit(0);
      break;

    case 'z':
      R -= 0.2;
      if (R < 0.2)
        R = 0.2;
      break;

    case 'x':
      R += 0.2;
      break;

    case 'w':
      Theta += 5 * M_PI/ 180.00;
      if (Theta >= M_PI / 2) Theta = M_PI / 2;
      break;

    case 's':
        Theta -= 5 * M_PI/ 180.00;
        if (Theta < -M_PI / 2) Theta = -M_PI / 2;
        break;

    case 'a':
		Phi += 5 * M_PI/ 180.00;
		break;

    case 'd':
		Phi -= 5 * M_PI/ 180.00;
		break;


    case ' ':
      saveScreenToFile = 1 - saveScreenToFile;
      break;
  }
}
