#ifndef _RAYTRACER_H_
#define _RAYTRACER_H_
#include <stdio.h>
#include <string>
#include <sstream>
#include <math.h>
#include <vector>
#include "openGL-headers.h"
#include "input.h"
#include "anamorph.h"
#include "render.h"
#include "performanceCounter.h"

extern int displayWidth;
extern int displayHeight;
extern double textureScale;
extern double fov;

extern Pic *pic;

#define MAX_TIME 99999999.00

struct Vect {
	double x;
	double y;
	double z;

	Vect() {
		x = 0; y = 0; z = 0;
	}
	Vect(double x, double y, double z) {
		this->x = x;this->y = y;this->z = z;
	}
	void Norm() {
		double magnitude = sqrt(x*x + y*y + z*z);
		this->x = this->x / magnitude;
		this->y = this->y / magnitude;
		this->z = this->z / magnitude;
	}

	Vect operator+(const Vect& b) {
		Vect result;
		result.x = this->x + b.x;
		result.y = this->y + b.y;
		result.z = this->z + b.z;
		return result;
	}

	Vect operator-(const Vect& b) {
		Vect result;
		result.x = this->x - b.x;
		result.y = this->y - b.y;
		result.z = this->z - b.z;
		return result;
	}

	void operator=(const Vect& b) {
		this->x = b.x;
		this->y = b.y;
		this->z = b.z;
	}

	double dot(const Vect& b) {
		return (this->x * b.x + this->y * b.y + this->z * b.z );
	}

	Vect cross(const Vect& b) {
		Vect result;
		result.x = this->y * b.z - this->z * b.y;
		result.y = this->z * b.x - this->x * b.z;
		result.z = this->x * b.y - this->y * b.x;
		return result;
	}


	Vect scalar(const double scalar) {
		Vect result;
		result.x = x * scalar;
		result.y = y * scalar;
		result.z = z * scalar;
		return result;
	}

	void print() {
		printf("X = %.3f, Y = %.3f, Z = %.3f\n", this->x, this->y, this->z);
	}
};

struct Ray {
	Vect origin;
	Vect direction;
};

class Object {
	public:
		Object() {}

		virtual bool checkIntersectionWithRay(const Ray& r, Vect &intersection) { return false;}

		virtual double getTimeOfIntersectionWithRay(const Ray& r) { return MAX_TIME;}

		virtual Vect getNormalAtPoint(double x, double y, double z) {Vect garbage; return garbage;}
};


class Plane : public Object {
	public:
		Vect point;
		Vect normal;

		bool checkIntersectionWithRay(const Ray& r, Vect &intersection) {
			if (normal.dot(r.direction) > 1e-6) return false;
			double t = getTimeOfIntersectionWithRay(r);
			intersection = ((Vect)r.direction).scalar(t) + r.origin;
			return true;
		}

		double getTimeOfIntersectionWithRay(const Ray& r) {
			double temp1 = normal.dot(r.direction);
			Vect temp2 = (point - r.origin);
			double result = temp2.dot(normal)/temp1;
			return result;
		}

		Vect getNormalAtPoint(double x, double y, double z) {
			return normal;
		}
};

class Sphere : public Object {
public:
	Vect center;
	double radius;

	bool checkIntersectionWithRay(const Ray& r, Vect &intersection) {
		double time = getTimeOfIntersectionWithRay(r);
		if (time >= MAX_TIME) {
			return false;
		}
		intersection = ((Vect)r.direction).scalar(time) + r.origin;
		return true;
	}

	double getTimeOfIntersectionWithRay(const Ray& r) {
		double a = SQ(r.direction.x) + SQ(r.direction.y) + SQ(r.direction.z);
		double b = 2.0 * (r.direction.x * (r.origin.x - center.x) + r.direction.y * (r.origin.y - center.y) + r.direction.z * (r.origin.z - center.z));
		double c = SQ(r.origin.x - center.x) + SQ(r.origin.y - center.y) + SQ(r.origin.z - center.z) - SQ(radius);
		double temp = SQ(b) - 4 * a * c;
		if (temp < 0) return MAX_TIME;

		double t1 = (-b + sqrt(temp))/(2.0);
		double t2 = (-b - sqrt(temp))/(2.0);
		double minT;
		if (t1 > t2) {minT = t2;}
		else {minT = t1;}
		return minT;
	}

	Vect getNormalAtPoint(double x, double y, double z) {
		Vect norm;
		norm.x = (x - center.x)/radius;
		norm.y = (y - center.y)/radius;
		norm.z = (z - center.z)/radius;
		norm.Norm();
		return norm;
	}

};


class Cylinder : public Object {
public:
	Plane bottom;
	Plane top;
	double radius;

	bool checkIntersectionWithRay(const Ray& r, Vect &intersection) {
		double time = getTimeOfIntersectionWithRay(r);
		if (time >= MAX_TIME) return false;
		intersection = ((Vect)r.direction).scalar(time) + r.origin;
		return true;
	}

	double getTimeOfIntersectionWithRay(const Ray& r) {
		bool intersectionWithCurvedSurface = false; double timeCurvedSurface;
		bool intersectionWithBottomPlane = false; double timeBottomPlane;
		bool intersectionWithTopPlane = false; double timeTopPlane;

		Vect i;
		if (bottom.checkIntersectionWithRay(r, i)) {
			if (SQ(i.x)+SQ(i.y) > SQ(radius)) {
				intersectionWithBottomPlane = false;
				timeBottomPlane = 1000.00;
			} else {
				intersectionWithBottomPlane = true;
				timeBottomPlane = bottom.getTimeOfIntersectionWithRay(r);
			}
		}

		if (top.checkIntersectionWithRay(r, i)) {
			if (SQ(i.x)+SQ(i.y) > SQ(radius)) {
				intersectionWithTopPlane = false;
				timeTopPlane = 1000.00;
			} else {
				intersectionWithTopPlane = true;
				timeTopPlane = top.getTimeOfIntersectionWithRay(r);
			}
		}

		double a = SQ(r.direction.x) + SQ(r.direction.y);
		double b = 2.0 * (r.direction.x * r.origin.x  + r.direction.y * r.origin.y);
		double c = SQ(r.origin.x) + SQ(r.origin.y) - SQ(radius);
		double temp = SQ(b) - 4 * a * c;
		if (temp < 0) {
			intersectionWithCurvedSurface = false;
			timeCurvedSurface = 1000.00;
		} else {
			// This means that ray intersected with curved surface.
			// Lets now check if it is in the bounds of two planes.
			double t1 = (-b + sqrt(temp))/(2.0 * a);
			double t2 = (-b - sqrt(temp))/(2.0 * a);
			double minT;
			if (t1 > t2) {minT = t2;}
			else {minT = t1;}
			if (minT < 0) {
				intersectionWithCurvedSurface = false;
				timeCurvedSurface = 1000.00;
			}
			Vect d = r.direction;
			Vect i = d.scalar(minT) + r.origin;
			if (i.z < bottom.point.z || i.z > top.point.z) {
				intersectionWithCurvedSurface = false;
				timeCurvedSurface = 1000.00;
			} else {
				intersectionWithCurvedSurface = true;
				timeCurvedSurface = minT;
			}
		}

		if (intersectionWithBottomPlane || intersectionWithTopPlane || intersectionWithCurvedSurface) {
			double minTime = timeBottomPlane;
			if (timeTopPlane < timeBottomPlane) {minTime = timeTopPlane;}
			if (timeCurvedSurface < minTime) { minTime = timeCurvedSurface;}
			return minTime;
		}
		return MAX_TIME;
	}

	Vect getNormalAtPoint(double x, double y, double z) {
		Vect norm;
		if (z < (top.point.z - 1e-6)) {
			norm.x = x/radius;
			norm.y = y/radius;
			norm.z = 0.0;
		} else {
			norm.x = 0.0;
			norm.y = 0.0;
			norm.z = 1.0;
		}
		norm.Norm();
		return norm;
	}
};




class Cone : public Object {
public:

	double height;
	double radius;

	bool checkIntersectionWithRay(const Ray& r, Vect &intersection) {
		double time = getTimeOfIntersectionWithRay(r);
		if (time >= MAX_TIME) return false;
		intersection = ((Vect)r.direction).scalar(time) + r.origin;
		return true;
	}

	double getTimeOfIntersectionWithRay(const Ray& r) {
		double k = radius/height;
		double A = SQ(r.direction.x) + SQ(r.direction.y) - SQ(k * r.direction.z);
		double B = 2 * r.origin.x * r.direction.x + 2 * r.origin.y * r.direction.y - 2 * SQ(k) * r.origin.z * r.direction.z  +  2 * SQ(k) * r.direction.z * height;
		double C = SQ(r.origin.x) + SQ(r.origin.y) - SQ(k * r.origin.z) + 2 * SQ(k) * r.origin.z * height - SQ(k * height);
		double temp = SQ(B) - 4 * A * C;
		if (temp > 0) {
			double t1 = (-B + sqrt(temp))/(2.0 * A);
			double t2 = (-B - sqrt(temp))/(2.0 * A);
			double minT;
			double maxT;
			if (t1 > t2) {minT = t2; maxT = t1;}
			else {minT = t1; maxT = t2;}
			Vect i = ((Vect)r.direction).scalar(minT) + r.origin;
			if (i.z > height || i.z < 0) {
				Vect i2 = ((Vect)r.direction).scalar(maxT) + r.origin;
				if (i2.z > height || i2.z < 0) {
					return MAX_TIME;
				} else {
					return maxT;
				}
			}
			return minT;
		}
		return MAX_TIME;
	}

	Vect getNormalAtPoint(double x, double y, double z) {
		Vect norm;
		double c = radius/height;
		norm.x = 2 * x;
		norm.y = 2 * y;
		norm.z = 2 * SQ(c) * height - 2 * z * SQ(c);
		norm.Norm();
		return norm;
	}
};


#endif
