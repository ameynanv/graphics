/*
 * rayTracerRenderer.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: amey
 */

#include "rayTracerRenderer.h"

Vect convertToWorldCoordinates(const Vect& b, double R, double theta, double phi) {
	Vect result;
	Vect eye;
	eye.x = R * cos(theta) * cos(phi);
	eye.y = R * cos(theta) * sin(phi);
	eye.z = R * sin(theta);
	Vect focus; focus.x = 0.0; focus.y = 0.0; focus.z = 0.0;
	Vect u; u.x = 0.0; u.y = 0.0; u.z = 1.0;
	Vect n = focus - eye; n.Norm();

	double u_dot_n = u.dot(n);
	Vect n_ = n.scalar(u_dot_n);
	Vect v = u - n_; v.Norm();
	Vect w = n.cross(v);

	result.x = w.x * b.x + v.x * b.y - n.x * b.z + eye.x;
	result.y = w.y * b.x + v.y * b.y - n.y * b.z + eye.y;
	result.z = w.z * b.x + v.z * b.y - n.z * b.z + eye.z;
	return result;
}

// This function assumes x is increasing from left to right and y is increasing form top to bottom
Ray createRay(int xPix, int yPix, double R, double theta, double phi, double fovDeg, int displayWidth, int displayHeight) {
	Ray result;

	float fov = fovDeg * M_PI/ 180.0;
	float aspectRatio = (1.0 * displayWidth) / (1.0 * displayHeight);
	result.direction.x = (xPix - displayWidth/2.0) * aspectRatio * tan(fov/2.0) * 2.0 / (displayWidth * 1.0);
	result.direction.y = (displayHeight/2.0 - yPix) * tan(fov/2.0) * 2.0 / (displayHeight*1.0) ;
	result.direction.z = -1.0;

	result.origin = convertToWorldCoordinates(result.origin, R, theta, phi);
	result.direction = convertToWorldCoordinates(result.direction, R, theta, phi);

	result.direction = result.direction - result.origin;
	result.direction.Norm();

	return result;
}


void rayTraceScene() {

	// Draw the texture on the XY plane.
	glBegin(GL_TRIANGLES);
		glNormal3f(0.0,0.0,-1.0);
		glTexCoord2f(0, 0); glVertex3f( -setup.textureScale/2.0, -setup.textureScale/2.0,	0.0);
		glTexCoord2f(0, 1); glVertex3f(  setup.textureScale/2.0, -setup.textureScale/2.0,	0.0);
		glTexCoord2f(1, 1); glVertex3f(  setup.textureScale/2.0,  setup.textureScale/2.0,	0.0);

		glTexCoord2f(0, 0); glVertex3f( -setup.textureScale/2.0, -setup.textureScale/2.0,	0.0);
		glTexCoord2f(1, 1); glVertex3f(  setup.textureScale/2.0,  setup.textureScale/2.0,	0.0);
		glTexCoord2f(1, 0); glVertex3f( -setup.textureScale/2.0,  setup.textureScale/2.0,	0.0);

	glEnd();
	glDisable(GL_TEXTURE_2D);

	// Raytracing begins now
	// Algorithm for raytracing:
	// 1) For every pixel in image plane i.e. your screen, generate a ray. Convert this ray into world coordinate system.
	// 2) Project the ray and check if it intersects with any objects.
	// 3) In case it intersects with an object, find out point of intersection and equation of normal to surface at that point.
	// 4) Use the equation of normal and incident ray direction to find out direction for reflected ray.
	// 5) Reflected ray will be originating from the point of intersection. Check if this reflected ray intersects the horizontal X-Y plane.
	// 6) If the ray intersects X-Y plane, then note down the value of the point of intersection.
	// 7) Do a look up with the anamorphed input image to find out the color of that point in the texture.
	// 8) Assign the color to the reflected ray's origin.
	glBegin(GL_POINTS);
		glPointSize(1.0);

		Sphere sphere;
		sphere.center = Vect(0,0,0.5);
		sphere.radius = 0.5;

		Cylinder cylinder;
		cylinder.bottom.point =  Vect(0, 0, 0.0);
		cylinder.bottom.normal =  Vect(0, 0, 1.0);
		cylinder.top.point =  Vect(0, 0, 2.0);
		cylinder.top.normal =  Vect(0, 0, 1.0);
		cylinder.radius = 1.0;

		Cone cone;
		cone.radius = setup.cone_radius;
		cone.height = setup.cone_radius / tan(setup.cone_angleSetpoint/2.0 * M_PI/180.0);

		std::vector<Object*> listOfObjects;
		if (setup.type == CYLINDRICAL_FINITE || setup.type == CYLINDRICAL_INFINITE) {
			listOfObjects.push_back(&cylinder);
		} else if (setup.type == CONICAL_FINITE || setup.type == CONICAL_INFINITE) {
			listOfObjects.push_back(&cone);
		}


		// Do not touch this object. This is XY plane
		Plane xyPlane;
		xyPlane.point = Vect(0, 0, 0);
		xyPlane.normal = Vect(0, 0, 1.0);

		for (int x = 0; x < displayWidth; x++) {
			for (int y = 0; y < displayHeight; y++) {
				//int x = displayWidth/2.0;
				//int y = displayHeight/2.0;
				//
				// Given x,y position on the image plane. i.e. your screen,
				// Generate corresponding ray. The equation for ray can be derived using the
				// fov (Field of view) and aspect ratio (displayWidth/displayHeight)
				// fov should be the same as used in perspective projection.
				// Source for the equations: http://run.usc.edu/cs420-s15/lec15-ray-tracing/15-ray-tracing.pdf Slide#10
				//
				Ray ray = createRay(x, y, R, Theta, Phi, fov, displayWidth,	displayHeight);
				ray.direction.Norm();

				Vect intersection;

				for (int index = 0; index < listOfObjects.size(); index++) {
					Object *obj = listOfObjects.at(index);

					if (obj->checkIntersectionWithRay(ray, intersection)) {
						//printf("Intersection :"); intersection.print();
						double r,g,b;
						r = 0.0; g = 256.0; b = 0.0; // This is default color for the cylinder

						Vect norm = obj->getNormalAtPoint(intersection.x, intersection.y, intersection.z);

						Ray reflectedRay; reflectedRay.origin = intersection;
						Vect incidentRay = ray.direction.scalar(-1.0);
						double temp = incidentRay.dot(norm);
						Vect temp2 = norm.scalar(2*temp);
						reflectedRay.direction = temp2 - incidentRay;
						reflectedRay.direction.Norm();

						Vect secondIntersection;
						// Find the intersection of the reflected ray with the X-Y Plane
						if (xyPlane.checkIntersectionWithRay(reflectedRay, secondIntersection)) {

								// Do manual texture mapping
								// We display texture on the X-Y on quad with corners (-textureScale/2.0, -textureScale/2.0) to (textureScale/2.0, textureScale/2.0)
								int xTex = (secondIntersection.x / setup.textureScale + 0.5) * pic->nx;
								int yTex = (secondIntersection.y / setup.textureScale + 0.5) * pic->ny;
								if (xTex > 0 && xTex < pic->nx && yTex > 0 && yTex < pic->ny) {
									int index = xTex* pic->nx * pic->bpp + yTex * pic->bpp;
									// We always blend the colors 0.5 to default color, 0.5 to reflected one.
									glColor3f((r * 0.5 + 0.5 * pic->pix[index]    )/256.0,
											  (g * 0.5 + 0.5 * pic->pix[index + 1])/256.0,
											  (b * 0.5 + 0.5 * pic->pix[index + 2])/256.0);
								} else {
									glColor3f((r * 0.5)/256.0,  (g * 0.5)/256.0, (b * 0.5)/256.0);
								}

						} else {
							glColor3f((r * 0.5)/256.0,  (g * 0.5)/256.0, (b * 0.5)/256.0);
						}
						glVertex3f(intersection.x, intersection.y, intersection.z);
					}
				}
			}
		}
	glEnd();
}
